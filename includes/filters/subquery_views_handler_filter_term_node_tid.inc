<?php

/**
 * Class to support switching to subquery mode.
 */
class subquery_views_handler_filter_term_node_tid extends views_handler_filter_term_node_tid {
  function init(&$view, &$options) {
    parent::init($view, $options);

    // Change over to our helper class if we've been requested to.
    if ($this->options['subquery']) {
      $this->helper = new views_many_to_one_filter_subqueries_helper($this);
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['subquery'] = array('default' => FALSE, 'bool' => TRUE);

    return $options;
  }

  function extra_options_form(&$form, &$form_state) {
    parent::extra_options_form($form, $form_state);

    $form['subquery'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use a subquery instead of joins for the filter query.'),
      '#decription' => t('Warning, not compatible with all options and settings.'),
      '#default_value' => $this->options['subquery'],
    );
  }

}
