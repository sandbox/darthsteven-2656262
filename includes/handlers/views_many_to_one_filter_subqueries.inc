<?php

class views_many_to_one_filter_subqueries_helper extends views_many_to_one_helper {
  function ensure_my_table() {
    if (!isset($this->handler->table_alias)) {
      // If the operator is anything other than AND, revert to the default behavior.
      if ($this->handler->operator != 'and') {
        return parent::ensure_my_table();
      }
    }
  }

  function add_filter() {
    if (empty($this->handler->value)) {
      return;
    }
    $this->handler->ensure_my_table();

    if ($this->handler->operator != 'and') {
      return parent::add_filter();
    }
    else {

      // Shorten some variables:
      $field = $this->get_field();
      $options = $this->handler->options;
      $value = $this->handler->value;
      $join = $this->get_join();
      if (empty($options['group'])) {
        $options['group'] = 0;
      }

      // Count the values that we're filtering on.
      $value_count = count($value);

      // Construct a subquery for this field.
      $subquery_alias = $this->handler->relationship . '_' . $this->handler->table;
      $subquery = db_select($join->table, $subquery_alias);
      // Return a single value in our subquery, the count of rows.
      $subquery->addExpression('count(*)');

      // Do the equivalent of joining in the table to the query. For each of
      // the rows in the query, we're going to limit the search range of our
      // subquery down to those rows that match the row in the outer query.
      $subquery->where($subquery_alias . '.' . $join->field . ' = ' . $this->handler->relationship . '.' . $join->left_field);

      // Filter the subquery on the filter values.
      $placeholder = $this->placeholder();
      $subquery->where($subquery_alias . $field . ' IN (' . $placeholder . ')', array($placeholder => $value));

      // Add the subquery as an expression to the main views query, passing in
      // our arguments. Note how we assert that the count of filter values is
      // equal to the count of rows returned by our subquery, this asserts the
      // AND part of our query.
      $placeholder = $this->placeholder();
      $this->handler->query->add_where_expression($options['group'], "($subquery) = " . $placeholder, array_merge($subquery->getArguments(), array($placeholder => $value_count)));
    }
  }
}
