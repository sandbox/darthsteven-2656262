<?php

/**
 * Implements hook_field_views_data_alter().
 */
function views_many_to_one_filter_subqueries_field_views_data_alter(&$data, $field, $module) {
  $default_data = field_views_field_default_views_data($field);
  foreach ($default_data as $table_name => $table_data) {
    foreach ($table_data as $field_name => $field_data) {
      if (isset($field_data['filter']) && $field_name != 'delta') {
        if (isset($data[$table_name][$field_name]['filter']['handler']) && ($data[$table_name][$field_name]['filter']['handler'] == 'views_handler_filter_term_node_tid')) {
          $data[$table_name][$field_name]['filter']['handler'] = 'subquery_views_handler_filter_term_node_tid';
        }
      }
    }
  }
}
